package com.itheima.health.service;

import com.itheima.health.pojo.OrderSetting;

import java.util.Date;
import java.util.List;

/**
 * @Description OrderSettingService
 * @Author W_yl
 * @Date 2024-06-27
 */
public interface OrderSettingService {

     void editNumberByOrderDate(OrderSetting orderSetting) ;
     void add(List<OrderSetting> list);

    List<OrderSetting> getOrderSettingByMonth(Date month);

}
