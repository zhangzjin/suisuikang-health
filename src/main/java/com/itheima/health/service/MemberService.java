package com.itheima.health.service;

import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Member;

import java.util.Map;

/**
 * @Description MemberService
 * @Author Zzjin
 * @Date 2024-06-28
 */
public interface MemberService {

    void add(Member member);

    Result findByTelephone(Map<String, String> map);

}
