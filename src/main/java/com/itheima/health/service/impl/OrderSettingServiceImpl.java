package com.itheima.health.service.impl;

import com.itheima.health.mapper.OrderSettingMapper;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.OrderSettingService;
import com.itheima.health.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
@Transactional
public class OrderSettingServiceImpl implements OrderSettingService {

    @Autowired
    private OrderSettingMapper orderSettingMapper;


    @Override
    public void add(List<OrderSetting> list) {
        for (OrderSetting orderSetting : list){
            OrderSetting soderSetting = orderSettingMapper.findByOrderDate(orderSetting.getOrderDate());
//            判断数据库中是否有该日期的预约设置
//            存在就修改，不存在就新增
            if(soderSetting != null){

                log.info("修改预约设置",orderSetting.getNumber());
                orderSettingMapper.editNumberByOrderDate(orderSetting);
            }else {
                log.info("新增预约设置",orderSetting.getNumber());
                orderSettingMapper.add(orderSetting);
            }

        }
    }

    @Override
    public List<OrderSetting> getOrderSettingByMonth(Date month) {
        Date begin = DateUtils.getFirstDay4Date(month);
        Date end = DateUtils.getLastDay4Date(month);
        System.out.println("begin:"+begin);
        System.out.println("end:"+end);
        List<OrderSetting> list = orderSettingMapper.getOrderSettingByMonth(begin,end);
        return list;
    }

    @Override
    public void editNumberByOrderDate(OrderSetting orderSetting) {
        OrderSetting sortOrderSetting = orderSettingMapper.findByOrderDate(orderSetting.getOrderDate());
        if(sortOrderSetting != null){
            orderSettingMapper.editNumberByOrderDate(orderSetting);
        }else {
            orderSettingMapper.add(orderSetting);
        }
    }
}