package com.itheima.health.service.impl;

import com.itheima.health.constant.MessageConstant;
import com.itheima.health.constant.RedisMessageConstant;
import com.itheima.health.dto.UserLoginDTO;
import com.itheima.health.dto.UserRegisterDTO;
import com.itheima.health.entity.Result;
import com.itheima.health.exception.BaseException;
import com.itheima.health.mapper.UserMapper;
import com.itheima.health.pojo.User;
import com.itheima.health.service.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class UserServiceImpl implements UserService {
    @Value("${suisui.md5.salt}")
    private String salt;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public Result register(User user){
        User rgsuser = userMapper.findByUsername(user.getUsername());
        if(rgsuser != null){
            return new Result(false, "注册失败, 用户名已被注册");
        }
        rgsuser =userMapper.findByTelephone(user.getTelephone());
        if(rgsuser != null){
            return new Result(false, "注册失败, 手机号已被注册");
        }
        String md5Password = DigestUtils.md5DigestAsHex((user.getPassword()+salt).getBytes());
        user.setPassword(md5Password);
        userMapper.register(user);
        return new Result(true, "注册成功");
//        User user = userMapper.findByUsername(userRegisterDTO.getUsername());
//        if(user != null){
//            throw new BaseException("用户名已被注册");
//        }
//        user =userMapper.findByTelephone(userRegisterDTO.getTelephone());
//        if(user != null){
//            throw new BaseException("手机号已被注册");
//        }
//        String md5Password = DigestUtils.md5DigestAsHex((userRegisterDTO.getPassword()+salt).getBytes());
//        userRegisterDTO.setPassword(md5Password);
//        userMapper.register(userRegisterDTO);
//        return new Result(true, "注册成功");
    }

    @Override
    public Result loginByPassword(UserLoginDTO userLoginDTO) {
        //1.校验用户名
        //根据用户账号查找用户对象
        User user = userMapper.findByUsername(userLoginDTO.getUsername());
        //判断用户对象是否为null，如果为null，抛出异常结束，提示“用户名或密码错误”
        if(user==null){
            throw new BaseException("用户名不存在");
        }
        //2.校验密码,如果两个密码不一样，抛出异常结束，提示“用户名或密码错误”
        //前端传来的密码：userLoginDTO.getPassword()  明文
        //数据库查询到的密码：user.getPassword() 密文
        //比较方案：对明文加密后得到密文再与数据库的密文对比
        //spring框架md5加密api语法：DigestUtils.md5DigestAsHex("字符串".getBytes());  得到密文
        String md5Password = DigestUtils.md5DigestAsHex((userLoginDTO.getPassword()+salt).getBytes());
//        byte[] bytes =(userLoginDTO.getPassword()+ salt).getBytes();
//        String md5Password = DigestUtils.md5DigestAsHex(bytes);
        if(!md5Password.equals(user.getPassword())){
            throw new BaseException("密码错误");
        }
        return new Result(true, "登录成功");

    }

    @Override
    public void send4Login(String telephone){
        String authCode = RandomStringUtils.randomNumeric(4);
        System.out.println("验证码为: " + authCode);

        if (authCode != "") {
            // 3.1.短信发送成功，把验证码放到Redis中,存活时间5分钟
            ValueOperations<String, Object> ops = redisTemplate.opsForValue();
            ops.set(telephone + RedisMessageConstant.SENDTYPE_BACKEND_GETPWD, authCode, 5, TimeUnit.MINUTES);
            if(redisTemplate.opsForValue().get(telephone + RedisMessageConstant.SENDTYPE_BACKEND_GETPWD)==""){
                throw new BaseException("验证码发送失败");
            }
        } else {
            throw new BaseException("验证码发送失败");
        }
    }

    @Override
    public Result loginByTelephone(Map<String, String> map){
        User user=userMapper.findByTelephone(map.get("telephone"));
        if(user==null){
            throw new BaseException("用户名或密码错误");
        }
        String telephone = map.get("telephone");
        String userCode = map.get("smsCode");
        String authCode = (String) redisTemplate.opsForValue().get(telephone + RedisMessageConstant.SENDTYPE_BACKEND_GETPWD);

        // 5.如果Redis中没有验证码,提示用户验证码已过期
        if (authCode == null) {
            throw new BaseException("验证码已过期");
        }
        // 6.如果验证码不正确，提示用户验证码不正确
        if (!authCode.equals(userCode)) {
            throw new BaseException("验证码不正确");
        }

        // 7.将User信息保存到Redis，使用手机号作为key，保存时长为30分钟
        redisTemplate.opsForValue().set(telephone, user, 5, TimeUnit.MINUTES);
        // 8.删除Redis中的登录验证码
        redisTemplate.delete(telephone + RedisMessageConstant.SENDTYPE_BACKEND_GETPWD);

        return new Result(true, "登录成功");
    }
}