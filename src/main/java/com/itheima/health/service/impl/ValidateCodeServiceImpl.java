package com.itheima.health.service.impl;

import com.itheima.health.constant.MessageConstant;
import com.itheima.health.constant.RedisMessageConstant;
import com.itheima.health.exception.BaseException;
import com.itheima.health.service.ValidateCodeService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @Description ValidateCodeServiceImpl
 * @Author Zzjin
 * @Date 2024-06-28
 */
@Service
public class ValidateCodeServiceImpl implements ValidateCodeService {
    @Autowired
    private RedisTemplate redisTemplate;
    public void send4Login(String telephone){
        String authCode = RandomStringUtils.randomNumeric(4);
        System.out.println("验证码为: " + authCode);

        if (authCode != "") {
            // 3.1.短信发送成功，把验证码放到Redis中,存活时间5分钟
            ValueOperations<String, Object> ops = redisTemplate.opsForValue();
            ops.set(telephone + RedisMessageConstant.SENDTYPE_LOGIN, authCode, 5, TimeUnit.MINUTES);
        } else {
            throw new BaseException("验证码发送失败");
        }
    }
}
