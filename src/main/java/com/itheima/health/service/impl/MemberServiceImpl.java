package com.itheima.health.service.impl;

import com.itheima.health.constant.MessageConstant;
import com.itheima.health.constant.RedisMessageConstant;
import com.itheima.health.entity.Result;
import com.itheima.health.exception.BaseException;
import com.itheima.health.mapper.MemberMapper;
import com.itheima.health.pojo.Member;
import com.itheima.health.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Description MemberServiceImpl
 * @Author Zzjin
 * @Date 2024-06-28
 */
@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private MemberMapper memberMapper;
    @Value("${suisui.md5.salt}")
    private String salt;
    @Override
    public void add(Member member){
        if(member.getPassword()!=""){
            String md5Password = DigestUtils.md5DigestAsHex((member.getPassword()+salt).getBytes());
            member.setPassword(md5Password);
        }
        memberMapper.add(member);
    }
    @Override
    public Result findByTelephone(Map<String, String> map){
        String telephone = map.get("telephone");
        String validateCode = map.get("validateCode");

        // 2.从Redis中获取缓存的验证码
        String authCode = (String) redisTemplate.opsForValue().get(telephone + RedisMessageConstant.SENDTYPE_LOGIN);

        // 3.如果Redis中没有验证码,提示用户验证码已过期
        if (authCode == null) {
            throw new BaseException("用户验证码已过期");
        }
        // 4.如果验证码不正确，提示用户验证码不正确
        if (!authCode.equals(validateCode)) {
            throw new BaseException("用户验证码不正确");
        }
        // 5.如果验证码正确，则调用DAO根据手机号查询Member会员
        Member member = memberMapper.findByTelephone(telephone);

        if (member == null) {
            // 6.如果会员为空，则创建会员
            member = new Member();
            member.setPhoneNumber(telephone);
            member.setRegTime(new java.util.Date());
            memberMapper.add(member);
        }

        // 7.将会员信息保存到Redis，使用手机号作为key，保存时长为30分钟
        redisTemplate.opsForValue().set(telephone, member, 30, TimeUnit.MINUTES);
        return new Result(true, MessageConstant.LOGIN_SUCCESS);
    }

}
