package com.itheima.health.service;

import com.itheima.health.dto.UserLoginDTO;
import com.itheima.health.dto.UserRegisterDTO;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.User;

import java.util.Map;

public interface UserService {
    Result register(User user);

    Result loginByPassword(UserLoginDTO userLoginDTO);

    void send4Login(String telephone);

    Result loginByTelephone(Map<String, String> map);
}
