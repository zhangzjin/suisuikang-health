package com.itheima.health.service.Impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.mapper.CheckGroupMapper;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description CheckGroupServiceImpl
 * @Author LJsong
 * @Date 2024-06-28
 */
@Service
public class CheckGroupServiceImpl implements CheckGroupService {
    @Autowired
    private CheckGroupMapper checkGroupMapper;
    @Override
    public void add(CheckGroup checkGroup, Integer[] checkItemIds) {
        // 添加检查组
        checkGroupMapper.add(checkGroup);

        // 在中间表中指定检查组中包含的检查项
        setCheckGroupAndCheckItem(checkGroup.getId(), checkItemIds);
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page<CheckGroup> page = checkGroupMapper.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void delete(Integer id) {
        checkGroupMapper.delete(id);
    }

    @Override
    public CheckGroup findById(Integer id) {
        CheckGroup checkGroup = checkGroupMapper.findById(id);
        return checkGroup;
    }

    @Override
    public List<Integer> findCheckItemIdsByCheckGroupId(Integer checkGroupId) {
        //根据检查组id查询检查项
        List<Integer> checkItemIds = checkGroupMapper.findCheckItemIdsByCheckGroupId(checkGroupId);
        //返回检查组里面的检查项
        return checkItemIds;
    }

    @Override
    public void edit(CheckGroup checkGroup, Integer[] checkitemIds) {
        // 1.调用DAO 编辑检查组
        checkGroupMapper.edit(checkGroup);

        // 2.调用DAO 根据检查组id删除中间表数据（清理原有关联关系）
        checkGroupMapper.deleteAssociation(checkGroup.getId());

        // 3.调用DAO 在中间表中添加检查组中包含的检查项
        setCheckGroupAndCheckItem(checkGroup.getId(), checkitemIds);
    }

    public void setCheckGroupAndCheckItem(Integer checkGroupId, Integer[] checkItemIds) {
        if (checkItemIds == null || checkItemIds.length == 0) {
            System.out.println("用户没有选择检查项");
            return;
        }

        // 遍历得到每个检查项id
        for (Integer checkItemId : checkItemIds) {
            // 调用Mapper往数据新增一个检查组id和一个检查项id
            checkGroupMapper.setCheckGroupAndCheckItem(checkGroupId, checkItemId);
        }
    }
}
