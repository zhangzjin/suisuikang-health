package com.itheima.health.service.Impl;

import com.itheima.health.mapper.MemberMapper;
import com.itheima.health.mapper.OrderMapepr;
import com.itheima.health.service.ReportService;
import com.itheima.health.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description ReportServiceImpl
 * @Author LJsong
 * @Date 2024-06-29
 */
@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private OrderMapepr orderMapepr;


    @Override
    public Map<String, Object> getBusinessReport() throws Exception {
        // 1.获得当前日期
        Date today = DateUtils.getToday();

        // 2.获得本周一的日期
        Date thisWeekMonday = DateUtils.getThisWeekMonday();

        // 3.获得本月第一天的日期
        Date firstDay4ThisMonth = DateUtils.getFirstDay4ThisMonth();

        // 4.今日新增会员数
        Integer todayNewMember = memberMapper.findMemberCountByDate(today);

        // 5.总会员数
        Integer totalMember = memberMapper.findMemberTotalCount();

        // 6.本周新增会员数
        Integer thisWeekNewMember = memberMapper.findMemberCountAfterDate(thisWeekMonday);

        // 7.本月新增会员数
        Integer thisMonthNewMember = memberMapper.findMemberCountAfterDate(firstDay4ThisMonth);

        // 8.今日预约数
        Integer todayOrderNumber = orderMapepr.findOrderCountByDate(today);

        // 9.今日到诊数
        Integer todayVisitsNumber = orderMapepr.findVisitsCountByDate(today);

        // 10.本周预约数
        Integer thisWeekOrderNumber = orderMapepr.findOrderCountAfterDate(thisWeekMonday);

        // 11.本周到诊数
        Integer thisWeekVisitsNumber = orderMapepr.findVisitsCountAfterDate(thisWeekMonday);

        // 12.本月预约数
        Integer thisMonthOrderNumber = orderMapepr.findOrderCountAfterDate(firstDay4ThisMonth);

        // 13.本月到诊数
        Integer thisMonthVisitsNumber = orderMapepr.findVisitsCountAfterDate(firstDay4ThisMonth);

        // 14.热门套餐（取前4） {name:'阳光爸妈升级肿瘤12项筛查（男女单人）体检套餐',setmeal_count:200,proportion:0.222}
        List<Map> hotSetmeal = orderMapepr.findHotSermeal();

        Map<String, Object> result = new HashMap<>();
        result.put("reportDate", DateUtils.getTodayString());
        result.put("todayNewMember", todayNewMember);
        result.put("totalMember", totalMember);
        result.put("thisWeekNewMember", thisWeekNewMember);
        result.put("thisMonthNewMember", thisMonthNewMember);
        result.put("todayOrderNumber", todayOrderNumber);
        result.put("todayVisitsNumber", todayVisitsNumber);
        result.put("thisWeekOrderNumber", thisWeekOrderNumber);
        result.put("thisWeekVisitsNumber", thisWeekVisitsNumber);
        result.put("thisMonthOrderNumber", thisMonthOrderNumber);
        result.put("thisMonthVisitsNumber", thisMonthVisitsNumber);
        result.put("hotSetmeal", hotSetmeal);

        return result;
    }

}
