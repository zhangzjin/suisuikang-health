package com.itheima.health.service;

import java.util.Map;

/**
 * @Description ReportService
 * @Author LJsong
 * @Date 2024-06-29
 */
public interface ReportService {
    Map<String, Object> getBusinessReport() throws Exception;
}
