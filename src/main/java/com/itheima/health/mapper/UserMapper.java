package com.itheima.health.mapper;

import com.itheima.health.dto.UserRegisterDTO;
import com.itheima.health.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
    @Select("select * from t_user where username=#{username}")
    User findByUsername(String username);

    @Select("select * from t_user where telephone=#{telephone}")
    User findByTelephone(String telephone);

    @Insert("insert into t_user(username,password,telephone) values (#{username},#{password},#{telephone})")
    void register(User user);


}
