package com.itheima.health.mapper;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.CheckItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description CheckItemMapper
 * @Author LJsong
 * @Date 2024-06-27
 */
@Mapper
public interface CheckItemMapper {
    void add(CheckItem checkItem);

    Page<CheckItem> findPage(String queryString);

    void delete(Integer id);

    CheckItem findById(Integer id);

    void edit(CheckItem checkItem);

    List<CheckItem> findAll();
}
