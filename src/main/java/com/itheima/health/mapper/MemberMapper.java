package com.itheima.health.mapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import com.itheima.health.pojo.Member;
import org.apache.ibatis.annotations.Insert;
import java.util.Date;

/**
 * @Description MemberMapper
 * @Author LJsong
 * @Date 2024-06-29
 */
@Mapper
public interface MemberMapper {
    /**
     * 今日新增会员数
     * @param today
     * @return
     */
    @Select("select count(*) from t_member where regTime = date(#{today});")
    Integer findMemberCountByDate(Date today);

    /**
     * 总会员数
     * @return
     */
    @Select("select count(*) from t_member;")
    Integer findMemberTotalCount();

    /**
     * 查询指定时间之后的会员数量
     * @param date
     * @return
     */
    @Select("select count(*) from t_member where regTime >= date(#{date});")
    Integer findMemberCountAfterDate(Date date);

/**
 * @Description MemberMapper
 * @Author Zzjin
 * @Date 2024-06-28
 */

    @Select("select * from t_member where phoneNumber=#{telephone};")
    Member findByTelephone(String telephone);

    @Insert("insert into t_member values (null, #{fileNumber}, #{name}, #{sex}, #{idCard}, #{phoneNumber}, #{regTime}, #{password}, #{email}, #{birthday}, #{remark});")
    void add(Member member);
}
