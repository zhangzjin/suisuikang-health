package com.itheima.health.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description OrderMapepr
 * @Author LJsong
 * @Date 2024-06-29
 */
@Mapper
public interface OrderMapepr {
    /**
     * 今日预约数
     * @param today
     * @return
     */
    @Select("select count(*) from t_order where orderDate = date(#{today});")
    Integer findOrderCountByDate(Date today);

    /**
     * 今日到诊数
     * @param today
     * @return
     */
    @Select("select count(*) from t_order where orderDate = date(#{today}) and orderStatus = '已到诊';")
    Integer findVisitsCountByDate(Date today);

    /**
     * 指定日期后的预约数
     * @param date
     * @return
     */
    @Select("select count(*) from t_order where orderDate >= date(#{date});")
    Integer findOrderCountAfterDate(Date date);

    /**
     * 指定日期后的到诊数
     * @param date
     * @return
     */
    @Select("select count(*) from t_order where orderDate >= date(#{date}) and orderStatus = '已到诊';")
    Integer findVisitsCountAfterDate(Date date);

    /**
     * 热门套餐
     * @return  {name:'阳光爸妈升级肿瘤12项筛查（男女单人）体检套餐',setmeal_count:200,proportion:0.222}
     */
    @Select("select t_setmeal.name, count(*) setmeal_count, count(*) / (select count(*) from t_order) proportion from t_setmeal inner join t_order on t_setmeal.id = t_order.setmeal_id group by t_setmeal.name limit 4;")
    List<Map> findHotSermeal();
}
