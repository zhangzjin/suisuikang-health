package com.itheima.health.mapper;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.CheckGroup;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description CheckGroupMapper
 * @Author LJsong
 * @Date 2024-06-28
 */
@Mapper
public interface CheckGroupMapper {
    Integer findCountByCheckItemId(Integer id);

    void add(CheckGroup checkGroup);

    void setCheckGroupAndCheckItem(Integer checkGroupId, Integer checkItemId);

    Page<CheckGroup> findPage(String queryString);

    void delete(Integer id);

    CheckGroup findById(Integer id);

    List<Integer> findCheckItemIdsByCheckGroupId(Integer checkGroupId);

    void edit(CheckGroup checkGroup);

    void deleteAssociation(Integer id);
}
