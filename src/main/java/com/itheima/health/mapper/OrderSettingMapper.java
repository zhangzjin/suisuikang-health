package com.itheima.health.mapper;

import com.itheima.health.pojo.OrderSetting;
import org.apache.ibatis.annotations.Mapper;


import java.util.Date;
import java.util.List;

/**
 * @Description OrderSettingMapper
 * @Author W_yl
 * @Date 2024-06-27
 */

@Mapper
public interface OrderSettingMapper {

    OrderSetting findByOrderDate(Date orderDate);

    void add(OrderSetting orderSetting);


    List<OrderSetting> getOrderSettingByMonth(Date begin, Date end);

    void editNumberByOrderDate(OrderSetting orderSetting);
}
