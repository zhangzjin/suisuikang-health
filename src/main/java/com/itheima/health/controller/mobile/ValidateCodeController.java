package com.itheima.health.controller.mobile;

import com.itheima.health.entity.Result;
import com.itheima.health.service.ValidateCodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description ValidateCodeController
 * @Author Zzjin
 * @Date 2024-06-28
 */
@RestController
@Slf4j
@RequestMapping("/validatecode")
public class ValidateCodeController {
    @Autowired
    private ValidateCodeService validateCodeService;
    @GetMapping("/send4Login.do")
    public Result send4Login(String telephone){
        log.info("发送短信验证码：{}",telephone);
        validateCodeService.send4Login(telephone);
        return new Result(true,"发送成功");
    }

}
