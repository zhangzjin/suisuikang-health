package com.itheima.health.controller.backend;

import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.service.CheckGroupService;
import com.itheima.health.service.CheckItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description CheckGroupController
 * @Author LJsong
 * @Date 2024-06-28
 */
@RestController
@Slf4j
@RequestMapping("/checkgroup")
public class CheckGroupController {
    @Autowired
    private CheckGroupService checkGroupService;
    @PostMapping("/add.do")
    public Result add(@RequestBody CheckGroup checkGroup, Integer[] checkitemIds){//checkitemIds
        checkGroupService.add(checkGroup,checkitemIds);
        return new Result(true,"新增成功");
    }
    @PostMapping("/findPage.do")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = checkGroupService.findPage(queryPageBean);
        return pageResult;
    }
    @GetMapping("/delete.do")
    public Result delete(Integer id){
        checkGroupService.delete(id);
        return new Result(true,"删除成功");
    }
    @GetMapping("/findById.do")
    public Result findById(Integer id) {
        CheckGroup checkGroup = checkGroupService.findById(id);
        return new Result(true,null, checkGroup);
    }

    @GetMapping("/findCheckItemIdsByCheckGroupId.do")
    public Result findCheckItemIdsByCheckGroupId(Integer checkGroupId){
        //根据checkGroupId查询对应所有的checkItemId
        List<Integer> checkItemIds = checkGroupService.findCheckItemIdsByCheckGroupId(checkGroupId);
        return new Result(true,null,checkItemIds);
    }
    @PostMapping("/edit.do")
    private Result edit(@RequestBody CheckGroup checkGroup,Integer[] checkitemIds){
        checkGroupService.edit(checkGroup,checkitemIds);
        return new Result(true,"编辑成功");
    }
}