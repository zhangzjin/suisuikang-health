package com.itheima.health.controller.backend;

import com.itheima.health.constant.JwtClaimsConstant;
import com.itheima.health.dto.UserLoginDTO;
import com.itheima.health.dto.UserRegisterDTO;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.User;
import com.itheima.health.properties.JwtProperties;
import com.itheima.health.service.UserService;
import com.itheima.health.utils.JwtUtil;
import com.itheima.health.vo.UserLoginVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private JwtProperties jwtProperties;

    @PostMapping("/register.do")
    public Result register(@RequestBody User user) {
        log.info("用户注册：{}", user);
        // 调用业务层注册
        Result result =userService.register(user);
        // 响应统一结果
        return result;
    }

    @PostMapping("/login.do")
    public Result loginByPassword(@RequestBody UserLoginDTO userLoginDTO) {
        //日志打印
        log.info("开始执行处理登录请求：{}",userLoginDTO);

        //调用业务处理登录请求
        Result result =  userService.loginByPassword(userLoginDTO);


//        //封装UserLoginVO对象返回
//        UserLoginVO userLoginVO = new UserLoginVO();
//        userLoginVO.setId(user.getId());
//
//        Map<String, Object> claims = new HashMap<>();
//        claims.put(JwtClaimsConstant.USER_ID, user.getId());
//
//        String token = JwtUtil.createJWT(
//                jwtProperties.getAdminSecretKey(),
//                jwtProperties.getAdminTtl(),
//                claims);
//
//        userLoginVO.setToken(token);
        return result;
    }

    @RequestMapping("/backendSend4Login.do")
    public Result backendSend4Login(String telephone) {

        log.info("发送短信验证码：{}", telephone);
        userService.send4Login(telephone);
        return new Result(true,"发送成功");
    }

    @RequestMapping("/telephoneLogin.do")
    public Result telephoneLogin(@RequestBody Map<String, String> map) {
        log.info("手机号登录：{}", map);
        Result result=userService.loginByTelephone(map);
        return result;
    }
}
