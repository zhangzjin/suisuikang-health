package com.itheima.health.controller.backend;

import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.OrderSettingService;
import com.itheima.health.utils.POIUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/ordersetting")
public class OrderSettingController {
    @Autowired
    private OrderSettingService orderSettingService;


    @RequestMapping("/upload.do")
    public Result upload(MultipartFile excelFile){

        try {
            //从EXCEL读取数据
            List<String[]> list = POIUtils.readExcel(excelFile);
            //创建集合用于封装数据
            List<OrderSetting> orderSettingList = new ArrayList<>();

            for (String[] strings : list){
                //将遍历数组集合，把数据传入OrderSetting对象中
                OrderSetting orderSetting = new OrderSetting(new Date(strings[0]),Integer.parseInt(strings[1]));

                orderSettingList.add(orderSetting);
            }
            //将数据存到集合中
            orderSettingService.add(orderSettingList);


        } catch (IOException e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.IMPORT_ORDERSETTING_FAIL);
        }
        return new Result(true, MessageConstant.IMPORT_ORDERSETTING_SUCCESS);
    }


    @RequestMapping("/getOrderSettingByMonth.do")
    public Result getOrderSettingByMonth(@DateTimeFormat(pattern = "yyyy-MM") Date month) {
        // 1.调用业务根据日期查询预约设置数据
        List<OrderSetting> list = orderSettingService.getOrderSettingByMonth(month);

        return new Result(true, MessageConstant.QUERY_ORDER_SUCCESS, list);
    }

    @RequestMapping("/editNumberByOrderDate.do")
    public Result editNumberByOrderDate(@RequestBody OrderSetting orderSetting){
        orderSettingService.editNumberByOrderDate(orderSetting);
        return new Result(true, MessageConstant.ORDERSETTING_SUCCESS);
    }


}