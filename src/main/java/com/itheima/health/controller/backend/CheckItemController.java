package com.itheima.health.controller.backend;

import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.service.CheckItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description CheckItemController
 * @Author LJsong
 * @Date 2024-06-27
 */
@Slf4j
@RestController
@RequestMapping("/checkitem")
public class CheckItemController {
    @Autowired
    private CheckItemService checkItemService;
    @PostMapping("/add.do")
    public Result add(@RequestBody CheckItem checkItem){
        checkItemService.add(checkItem);
        return new Result(true,"新增成功");
    }
    @RequestMapping("/findPage.do")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = checkItemService.findPage(queryPageBean);
        return pageResult;
    }
    @GetMapping("/delete.do")
    public Result delete(Integer id){
        checkItemService.delete(id);
        return new Result(true,"删除成功");
    }
    @GetMapping("/findById.do")
    public Result findById(Integer id) {
        // 1.调用业务层根据id查询检查项
        CheckItem checkItem = checkItemService.findById(id);
        // 2.响应统一结果Result,包含检查项数据
        return new Result(true, null, checkItem);
    }

    @PostMapping("/edit.do")
    public Result edit(@RequestBody CheckItem checkItem){
        checkItemService.edit(checkItem);
        return new Result(true,"修改成功");
    }
    @GetMapping("/findAll.do")
    public Result findAll() {
        List<CheckItem> checkItems = checkItemService.findAll();
        return new Result(true, null, checkItems);
    }
}
