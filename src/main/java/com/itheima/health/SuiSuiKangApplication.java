package com.itheima.health;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuiSuiKangApplication {
    public static void main(String[] args) {
        SpringApplication.run(SuiSuiKangApplication.class, args);
    }
}
