package com.itheima.health.interceptor;

import com.itheima.health.constant.JwtClaimsConstant;
import com.itheima.health.context.BaseContext;
import com.itheima.health.properties.JwtProperties;
import com.itheima.health.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class JwtTokenAdminInterceptor implements HandlerInterceptor {
    @Autowired
    private JwtProperties jwtProperties;

    @Override
    //请求资源前拦截，返回true表示放行，false表示拦截
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1.判段当前访问的资源是不是处理器方法资源（就是静态资源)，如果不是放行
        if (!(handler instanceof HandlerMethod)) {
            //进入这里说明访问的是静态资源，所以放行，因为静态资源不用校验身份
            return false;
        }
        //2.校验token
        // 2.1 从请求头中获取token数据
        String token=request.getHeader(jwtProperties.getAdminTokenName());
        //2.2 token数据有效，解析token,很到载荷
        if(token!=null&&!"".equals(token))
        {
            try {
                Claims claims = JwtUtil.parseJWT(jwtProperties.getAdminSecretKey(),token);

                Integer userId=(Integer) claims.get(JwtClaimsConstant.USER_ID);
                log.info("当前用户id为："+userId);
                //使用工具类存储当前用户id
                BaseContext.setCurrentId(userId);
                return true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        response.setStatus(401);
        return false;
    }
}
