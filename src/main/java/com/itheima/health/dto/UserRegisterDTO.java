package com.itheima.health.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description UserRegisterDTP
 * @Author Zzjin
 * @Date 2024-06-28
 */
@Data
public class UserRegisterDTO implements Serializable {
    //用户名
    private String username;

    //密码
    private String password;

    //电话
    private String telephone;
}
