package com.itheima.health.constant;

public class JwtClaimsConstant {

    public static final String USER_ID = "userId";
    public static final String WX_USER_ID = "wxUserId";
    public static final String PHONE = "phone";
    public static final String USERNAME = "username";
    public static final String NAME = "name";

}
